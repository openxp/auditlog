var portalLib = require('/lib/xp/portal');
var contentlLib = require('/lib/xp/portal');
var thymeleafLib = require('/lib/xp/thymeleaf');
var adminLib = require('/lib/xp/admin');
var appersist = require('/lib/openxp/appersist');

exports.get = function (req) {
    var view = resolve('toolstarter.html');

    var params = {
        adminUrl: adminLib.getBaseUri(),
        assetsUrl: portalLib.assetUrl({
            path: ''
        }),
        launcherPath: adminLib.getLauncherPath(),
        launcherUrl: adminLib.getLauncherUrl()
    };
    var query = {
        start: 0,
        count: 10000,
        filters: {
            boolean: {
                must: [
                    {
                        exists: {
                            field: "path"
                        }
                    }
                ]
            }
        }
    };
    var draftResults = appersist.repository.getConnection({repository:'auditlog', branch:'draft'}).query(query);
    var masterResults =  appersist.repository.getConnection({repository:'auditlog', branch:'master'}).query(query);
    var logEntries = [];
    draftResults.hits.forEach(function(draftResult){
        var entry = appersist.repository.getConnection({repository:'auditlog', branch:'draft'}).get(draftResult.id);
        if (entry){
            entry.branch = 'draft';
            entry.date = new Date(parseInt(entry.timestamp,10)).toISOString();
            logEntries.push(entry);
        }
    });
    masterResults.hits.forEach(function(masterResult){
        var entry = appersist.repository.getConnection({repository:'auditlog', branch:'master'}).get(masterResult.id);
        if (entry) {
            entry.branch = 'master';
            entry.date = new Date(parseInt(entry.timestamp,10)).toISOString();
            logEntries.push(entry);
        }
    });

    params.auditlogs = logEntries;

    return {
        body: thymeleafLib.render(view, params)
    };
};