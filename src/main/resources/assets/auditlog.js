$(document).ready(function() {
    $('#auditlogs').DataTable({
        "order": [[ 0, "desc" ]],
        "scrollY":        "600px",
        "scrollCollapse": true,
        "paging":         true
    });
} );