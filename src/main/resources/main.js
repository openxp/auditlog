
var eventLib = require('/lib/xp/event');
var appersist = require('/lib/openxp/appersist');
var contentLib = require('/lib/xp/content');

var eventlistener = eventLib.listener({
    type: 'node.*',
    localOnly: false,
    callback: function (event) {
        var type = event.type;
        var timestamp = event.timestamp;
        if (event.data && event.data.nodes){
            var draftNode = event.data.nodes.filter(function(node){return node.branch === 'draft' && node.repo !== 'auditlog'}).forEach(function(node){
                node.type = event.type;
                node.timestamp = event.timestamp;
                var content = contentLib.get({key: node.id, branch:'draft'});
                log.info("draft content %s",content);
                if (content){
                    node.modifier = content.modifier;
                    node.displayName = content.displayName;
                    node.contentType = content.type;
                }
                appersist.repository.getConnection({repository:'auditlog', branch:'draft'}).create(node);
            });
            var masterNode = event.data.nodes.filter(function(node){return node.branch === 'master' && node.repo !== 'auditlog'}).forEach(function(node){
                node.type = event.type;
                node.timestamp = event.timestamp;
                var content = contentLib.get({key: node.id, branch:'draft'});
                log.info("master content %s",content);
                if (content){
                    node.modifier = content.modifier;
                    node.displayName = content.displayName;
                    node.contentType = content.type;
                }
                appersist.repository.getConnection({repository:'auditlog', branch:'master'}).create(node);
            });
        }

    }
});
exports.get = function (req) {
    log.info("auditlog main js");
};




